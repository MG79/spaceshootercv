using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DealDamage : MonoBehaviour
{
    [SerializeField] int damage = 1;
    [SerializeField] bool destroyOnCollision = true;
    PlayerScoreController playerScoreController;

    public int ReturnDamage()
    {
        return damage;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if(destroyOnCollision==true)Destroy(gameObject); //stare niszczy obiekt po wyj�ciu za ekran
        if (destroyOnCollision == true) gameObject.SetActive(false);
    }
}
