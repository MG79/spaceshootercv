using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectShortFlash : MonoBehaviour
{
    protected SpriteRenderer renderer;
    protected Color defaultColor;
    [SerializeField] protected Color flashColor=Color.red;
    [SerializeField] protected float flashDuration = 0.1f;

    protected virtual void Start()
    {
        renderer = gameObject.GetComponent<SpriteRenderer>();
        defaultColor = gameObject.GetComponent<SpriteRenderer>().color;
    }

    protected virtual void OnTriggerEnter2D(Collider2D collider)
    {
        FlashOnHit();
    }


    protected IEnumerator RestoreDefaultMaterial()
    {
        yield return new WaitForSeconds(flashDuration);
        renderer.color = defaultColor;
    }

    protected virtual void FlashOnHit()
    {
        renderer.color = flashColor;
        StartCoroutine(RestoreDefaultMaterial());
    }
}
