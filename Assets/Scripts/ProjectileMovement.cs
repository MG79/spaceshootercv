using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMovement : MonoBehaviour
{
    [SerializeField] bool moveDownward = true;
    [SerializeField] float projectileSpeed = 5f;

    private void Update()
    {
        ProjectileMovementMethod();
    }

    void ProjectileMovementMethod()
    {
        if(moveDownward==true)
        {
            transform.position = transform.position - new Vector3(0, 1, 0) * projectileSpeed * Time.deltaTime;
        }
        else
        {
            transform.position = transform.position + new Vector3(0, 1, 0) * projectileSpeed * Time.deltaTime;
        }
    }
}
