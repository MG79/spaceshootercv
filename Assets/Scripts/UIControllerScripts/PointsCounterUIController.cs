using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using TMPro;

public class PointsCounterUIController : MonoBehaviour
{
    [SerializeField] GameObject playerObjectHandle;

    private void Update()
    {
        gameObject.GetComponent<TextMeshProUGUI>().text = playerObjectHandle.GetComponent<PlayerScoreController>().ReturnPlayerPointScore().ToString();
    }
}
