using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI; //biblioteka UI

public class EnemyHealthBarController : HealthBarController
{
    [SerializeField] protected GameObject enemyReference;

    protected override void Start()
    {
        slider = gameObject.GetComponent<Slider>();
        slider.maxValue = enemyReference.GetComponent<ObjectLifeControllerSpaceStation>().ReturnMaxLife();
    }

    protected override void Update()
    {
        slider.value = enemyReference.GetComponent<ObjectLifeControllerSpaceStation>().ReturnCurrentLife();
    }
}
