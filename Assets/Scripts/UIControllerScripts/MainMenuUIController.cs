using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

using System.IO;
using UnityEngine.UI;
using TMPro;

public class MainMenuUIController : MonoBehaviour
{
    [SerializeField] GameObject buttton;

    private void Start()
    {
        Time.timeScale = 1;
    }

    public void GoToLevelChoice()
    {
        SceneManager.LoadScene("LevelChoiceMenu");
    }

    public void QuitGameMainMenu()
    {
        Application.Quit();
    }
}
