using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class EndGameController : MonoBehaviour
{
    private void Start()
    {
        SaveController.WriteIntegerToFile(6);
    }

    public void GoToMainMenuAfterBeatingTheGame()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
