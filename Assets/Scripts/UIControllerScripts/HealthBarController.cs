using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI; //biblioteka UI

public class HealthBarController : MonoBehaviour
{
    [SerializeField] protected GameObject playerObjectHandle;
    protected Slider slider;

    protected virtual void Start()
    {
        slider = gameObject.GetComponent<Slider>();
        slider.maxValue = playerObjectHandle.GetComponent<PlayerLifeController>().ReturnPlayerMaxLifeCount();
    }

    protected virtual void Update()
    {
        slider.value = playerObjectHandle.GetComponent<PlayerLifeController>().ReturnPlayerCurrentLifeCount();
    }

}
