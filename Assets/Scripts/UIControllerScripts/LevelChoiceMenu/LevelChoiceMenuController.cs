using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
using System.IO;
using System;

public class LevelChoiceMenuController : MonoBehaviour
{
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // uchwyty do button�w leveli
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    [SerializeField] GameObject Level2Button;
    [SerializeField] GameObject Level3Button;
    [SerializeField] GameObject Level4Button;

    private void Start()
    {
        CheckIfLevelIsUnlocked();
    }

    void CheckIfLevelIsUnlocked()
    {
        if (SaveController.CheckIfFileExists())
        {
            int progress = SaveController.ReadIntegerFromFile();

            switch(progress)
            {
                case 5:
                    Level2Button.SetActive(true);
                    break;
                case 6:
                    Level2Button.SetActive(true);
                    Level3Button.SetActive(true);
                    break;
                case 7:
                    Level2Button.SetActive(true);
                    Level3Button.SetActive(true);
                    Level4Button.SetActive(true);
                    break;
            }
        }
    }

    public void Level1MenuChoiceButton()
    {
        SceneManager.LoadScene("Level1");
    }

    public void Level2MenuChoiceButton()
    {
        SceneManager.LoadScene("Level2");
    }

    public void Level3MenuChoiceButton()
    {
        SceneManager.LoadScene("Level3");
    }

    public void Level4MenuChoiceButton()
    {
        SceneManager.LoadScene("Level4");
    }
}
