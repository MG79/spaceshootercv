using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyProjectileOutOfScreen : MonoBehaviour
{
    Vector2 minBounds;
    Vector2 maxBounds;

    private void Start()
    {
        minBounds = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        maxBounds = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

        /*
        Debug.Log("minBounds.x = ");
        Debug.Log(minBounds.x);
        Debug.Log("minBounds.y = ");
        Debug.Log(minBounds.y);

        Debug.Log("maxBounds.x = ");
        Debug.Log(maxBounds.x);
        Debug.Log("maxBounds.y = ");
        Debug.Log(maxBounds.y);
        */
    }

    private void Update()
    {
        SetProjectileInactiveOutOfScreen();
        //DestroyProjectileOutOfScreenMethod(); //stara metoda
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // metoda z POOLERA
    //ustawia projectile na inactive je�li wyjdzie poza ekran
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void SetProjectileInactiveOutOfScreen()
    {
        if (gameObject.transform.position.x < minBounds.x || gameObject.transform.position.x > maxBounds.x)
        {
            gameObject.SetActive(false);
        }
        if (gameObject.transform.position.y < minBounds.y || gameObject.transform.position.y > maxBounds.y)
        {
            gameObject.SetActive(false);
        }
    }


    //stara metoda
    /*
    void DestroyProjectileOutOfScreenMethod()
    {
        if (gameObject.transform.position.x < minBounds.x || gameObject.transform.position.x > maxBounds.x)
        {
            Destroy(gameObject);
        }
        if (gameObject.transform.position.y < minBounds.y || gameObject.transform.position.y > maxBounds.y)
        {
            Destroy(gameObject);
        }
    }
    */
}
