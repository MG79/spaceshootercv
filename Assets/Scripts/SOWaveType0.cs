using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "EnemyWaveSO", fileName = "NewWaveSO")]
public class SOWaveType0 : ScriptableObject
{
    [SerializeField] List<GameObject> enemyPrefabList;
    [SerializeField] GameObject pathPrefab;

    [SerializeField] int enemyCount;
    public int EnemyCount
    {
        get
        {
            return enemyCount;
        }
    }

    [SerializeField] float enemySpawnGap;
    public float EnemySpawnGap
    {
        get
        {
            return enemySpawnGap;
        }
    }

    public List<Transform> ReturnListOfPathPoints()
    {
        List<Transform> listOfPathPoints = new List<Transform>();

        for(int i=0; i < pathPrefab.transform.childCount; i++)
        {
            listOfPathPoints.Add(pathPrefab.transform.GetChild(i));
        }

        return listOfPathPoints;
    }

    //wywoływana w WaveManagerze w funkcji CreateEnemiesInWaveKorutyna(int waveIndex) jako prefab wroga dla Instantiate()
    public GameObject ReturnRandomizedEnemyPrafab()
    {
        int enemyPrefabIndexInList = Random.Range(0, enemyPrefabList.Count-1);
        return enemyPrefabList[enemyPrefabIndexInList];
    }
}
