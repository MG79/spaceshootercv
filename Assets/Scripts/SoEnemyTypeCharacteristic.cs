using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "EnemyTypeTemplateSO", fileName = "NewEnemyTypeSO")]
public class SoEnemyTypeCharacteristic : ScriptableObject
{
    [SerializeField] float enemySpeed;

    public float EnemySpeed
    {
        get
        {
            return enemySpeed;
        }
    }

    [SerializeField] int enemyPointValue = 1;

    public int EnemyPointValue
    {
        get
        {
            return enemyPointValue;
        }
    }
    
    [SerializeField] int enemyHitPoints = 1;

    public int EnemyHitPoints
    {
        get
        {
            return enemyHitPoints;
        }
    }

    [SerializeField] float weaponReloadTimeMin = 2f;

    public float WeaponReloadTimeMin
    {
        get
        {
            return weaponReloadTimeMin;
        }
    }

    [SerializeField] float weaponReloadTimeMax = 6f;

    public float WeaponReloadTimeMax
    {
        get
        {
            return weaponReloadTimeMax;
        }
    }
}
