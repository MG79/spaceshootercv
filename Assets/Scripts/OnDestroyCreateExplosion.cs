using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDestroyCreateExplosion : MonoBehaviour
{
    [SerializeField] GameObject explosionPrefab;

    public void InstantiateExplosion()
    {
        Instantiate(explosionPrefab, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, 0), Quaternion.identity);
    }

    public void InstantiateExplosion(float x, float y)
    {
        Instantiate(explosionPrefab, new Vector3(x, y, 0), Quaternion.identity);
    }
}
