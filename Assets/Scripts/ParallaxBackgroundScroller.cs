using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBackgroundScroller : MonoBehaviour
{
    [SerializeField] Vector2 moveSpeed;
    Vector2 offset;
    [SerializeField] Material material;

    void Start()
    {
        
    }

    void Update()
    {
        offset = moveSpeed * Time.deltaTime;
        material.mainTextureOffset = material.mainTextureOffset + offset;
    }
}
