using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObjectAfterTimer : MonoBehaviour
{
    [SerializeField] float timeToDestroyObject = 3f;

    private void Start()
    {
        
    }

    IEnumerable TimerToDestroy()
    {
        yield return new WaitForSeconds(timeToDestroyObject);
        Destroy(gameObject);
    }
}
