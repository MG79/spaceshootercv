using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] protected bool setActive = true;
    public bool SetActive
    {
        set
        {
            setActive = value;
        }
    }

    //MoveTowardsExactPosition
    bool isMovingToExactPosition = false;
    Transform argTransform;
    float argSpeed;

    [Header("Zmienne Enemy")]
     [SerializeField] protected SoEnemyTypeCharacteristic enemyScriptableObject;
     [SerializeField] protected SOWaveType0 waveScriptableObject;

    protected int waypointsCount; //ilo�� waypoint�w
    protected int currentWaypointIndex;

    protected void Start()
    {
        SetParameters();
    }

    protected void Update()
    {

        if(setActive==true)
        {
            EnemyMovementMethod();
        }
        else if (isMovingToExactPosition == true)
        {
            MoveToPosition();
        }
    }

    protected void SetParameters()
    {
        waypointsCount = waveScriptableObject.ReturnListOfPathPoints().Count;
    }

    protected void EnemyMovementMethod()
    {
        if(currentWaypointIndex < waypointsCount)
        {
            transform.position = Vector2.MoveTowards(transform.position /*aktualna pozycja*/, waveScriptableObject.ReturnListOfPathPoints()[currentWaypointIndex].position /*pozycja do kt�rej sie poruszamy*/, enemyScriptableObject.EnemySpeed * Time.deltaTime /*szybko�� razy deltaTime*/);
            if (transform.position == waveScriptableObject.ReturnListOfPathPoints()[currentWaypointIndex].position)
            {
                currentWaypointIndex++;
            }
        }

        else if(currentWaypointIndex==waypointsCount)
        {
            currentWaypointIndex = 1;
            transform.position = waveScriptableObject.ReturnListOfPathPoints()[0].position;
        }
    }

    public void InitiateMoveToPosition(Transform _argTransform, float _argSpeed)
    {
        isMovingToExactPosition = true;
        argTransform = _argTransform;
        argSpeed = _argSpeed;
    }

    protected void MoveToPosition()
    {
        transform.position = Vector2.MoveTowards(transform.position /*aktualna pozycja*/, argTransform.position  /*pozycja do kt�rej sie poruszamy*/, argSpeed * Time.deltaTime /*szybko�� razy deltaTime*/);
        if (gameObject.transform.position == argTransform.position)
        {
            isMovingToExactPosition = false;
        }
    }

    public float ReturnSpeed()
    {
        return enemyScriptableObject.EnemySpeed;
    }
}
