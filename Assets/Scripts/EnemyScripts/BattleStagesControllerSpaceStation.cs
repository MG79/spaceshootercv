using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleStagesControllerSpaceStation : MonoBehaviour
{
    [SerializeField] GameObject bossCockpitHandle;
    [SerializeField] GameObject bossHandle;
    [SerializeField] GameObject bossTail;
    [SerializeField] GameObject bossWing;

    [SerializeField] ObjectLifeControllerSpaceStation objectLifeControllerSpaceStation;

    [SerializeField] GameObject stage2Wave;
    [SerializeField] GameObject stage3Wave;
    [SerializeField] GameObject stage4Wave1;

    bool stage2InProgress = false;
    bool stage3InProgress = false;
    bool stage4InProgress = false;

    Transform bossStartingTransform;

    bool setActiveExplosions = false;
    bool explosionCooldown = false;

    float maxExplosionSpawnTime = 4f;
    float minExplosionSpawnTime = 4f;

    private void Start()
    {
        bossStartingTransform = bossHandle.transform;
    }

    void StagesController()
    {
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Faza 4
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if ((float)objectLifeControllerSpaceStation.ReturnCurrentLife() < (float)objectLifeControllerSpaceStation.ReturnMaxLife() / 4 && stage4InProgress == false)
        {
            stage4InProgress = true;

            bossHandle.GetComponent<RotateEnemyAround>().Stage4RotateEnemyAround();

            SetActiveWaveForStage(stage4Wave1);
            //ReturnBossToDefaultPosition();

            minExplosionSpawnTime = 0.2f;
            maxExplosionSpawnTime = 1f;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Faza 3
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if ((float)objectLifeControllerSpaceStation.ReturnCurrentLife() < (float)objectLifeControllerSpaceStation.ReturnMaxLife() / 3 && stage2InProgress == false)
        {
            stage2InProgress = true;

            bossHandle.GetComponent<RotateEnemyAround>().Stage3RotateEnemyAround();

            SetBossMovementActive(true);

            SetActiveWaveForStage(stage3Wave);

            setActiveExplosions = true;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Faza 2
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if ((float)objectLifeControllerSpaceStation.ReturnCurrentLife() < (float)objectLifeControllerSpaceStation.ReturnMaxLife() / 1.5f && stage3InProgress == false)
        {
            stage3InProgress = true;

            bossHandle.GetComponent<RotateEnemyAround>().Stage2RotateEnemyAround();

            SetActiveWaveForStage(stage2Wave);
        }
    }

    private void Update()
    {
        StagesController();

        if(setActiveExplosions==true)
        {
            if(explosionCooldown==false)
            {
                StartCoroutine(CreateRandomFireCoroutine());
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Metody dla ruchu bossa po mapie
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void SetBossMovementActive(bool arg)
    {
        bossHandle.GetComponent<EnemyMovement>().SetActive = arg;
    }

    void ReturnBossToDefaultPosition()
    {
        SetBossMovementActive(false);
        bossHandle.GetComponent<EnemyMovement>().InitiateMoveToPosition(bossStartingTransform, bossHandle.GetComponent<EnemyMovement>().ReturnSpeed());
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Kolejne fale przeciwników
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void SetActiveWaveForStage(GameObject wave)
    {
        wave.SetActive(true);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Uszkodzenia statki przeciwnika
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
    void CreateRandomFire()
    {
        bossHandle.GetComponentInChildren<OnDestroyCreateExplosion>().InstantiateExplosion();
        bossHandle.GetComponentInChildren<ObjectLifeControllerSpaceStation>().PlayExplosionSound();
    }

    void CreateRandomFire(float x, float y)
    {
        bossHandle.GetComponentInChildren<OnDestroyCreateExplosion>().InstantiateExplosion(x, y);
        bossHandle.GetComponentInChildren<ObjectLifeControllerSpaceStation>().PlayExplosionSound();
    }


    IEnumerator CreateRandomFireCoroutine()
    {
        explosionCooldown = true;
        yield return new WaitForSeconds(Random.Range(minExplosionSpawnTime, maxExplosionSpawnTime));
        CreateRandomFire();
        yield return new WaitForSeconds(Random.Range(minExplosionSpawnTime, maxExplosionSpawnTime));
        CreateRandomFire(bossTail.transform.position.x, bossTail.transform.position.y);
        yield return new WaitForSeconds(Random.Range(minExplosionSpawnTime, maxExplosionSpawnTime));
        CreateRandomFire(bossWing.transform.position.x, bossWing.transform.position.y);

        explosionCooldown = false;
    }
}
