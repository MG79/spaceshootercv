using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateEnemyAround : MonoBehaviour
{
    [SerializeField] bool switchDirection = true;
    bool switchDirectionIsInProgress = false;

    [SerializeField] float rotationSpeed = 10f;
    float changeDirectionTimer = 60f;

    [SerializeField] float minTimeToSwitchDirection = 0.5f;
    [SerializeField] float maxTimeToSwitchDirection = 4f;
    [SerializeField] bool randomizeTimer = false;

    ObjectLifeControllerSpaceStation objectLifeControllerSpaceStation;

    bool stage2InProgress = false;
    bool stage3InProgress = false;
    bool stage4InProgress = false;

    private void Start()
    {
        objectLifeControllerSpaceStation = GetComponentInChildren<ObjectLifeControllerSpaceStation>();
    }

    private void Update()
    {
        transform.Rotate(0, 0, rotationSpeed * Time.deltaTime);

        if (switchDirection == true && switchDirectionIsInProgress == false)
        {
            switchDirectionIsInProgress = true;
            StartCoroutine(ChangeDirection());
        }
    }

    IEnumerator ChangeDirection()
    {
        if(randomizeTimer==false)yield return new WaitForSeconds(changeDirectionTimer);
        else if(randomizeTimer==true) yield return new WaitForSeconds(RandomizeRotationTimer());
        rotationSpeed = rotationSpeed * (-1);
        switchDirectionIsInProgress = false;
    }

    float RandomizeRotationTimer()
    {
        return Random.Range(minTimeToSwitchDirection, maxTimeToSwitchDirection);
    }

    public void Stage2RotateEnemyAround()
    {
        rotationSpeed = rotationSpeed * 4f;
        changeDirectionTimer = 20;
    }

    public void Stage3RotateEnemyAround()
    {
        rotationSpeed = rotationSpeed * 3f;
        randomizeTimer = true;
    }

    public void Stage4RotateEnemyAround()
    {
        rotationSpeed = rotationSpeed * 3f;
        randomizeTimer = true;
    }
}
