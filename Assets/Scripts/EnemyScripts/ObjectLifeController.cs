using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectLifeController : MonoBehaviour
{
    protected int lifeNumber;

    protected PlayerScoreController playerScoreController;
    
    [SerializeField] protected SoEnemyTypeCharacteristic enemyScriptableObject;

    [SerializeField] protected AudioClip explosionSoundClip;
    [SerializeField]  [Range(0f, 1f)] protected float explosionSoundVolume = 1f;

    [SerializeField] protected bool playExplosionSound = true;

    protected virtual void Awake()
    {
        lifeNumber = enemyScriptableObject.EnemyHitPoints;
        playerScoreController = FindObjectOfType<PlayerScoreController>();
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        //je�li collision nie ma komponentu DealDamage (skryptu) to dealDamage b�dzie null-em
        DealDamage dealDamage = collision.GetComponent<DealDamage>();


        if(dealDamage!=null)
        {
            lifeNumber = lifeNumber - dealDamage.ReturnDamage();
            if (lifeNumber <= 0)
            {
                if (gameObject.tag == "Enemy") playerScoreController.IncreasePlayerScore(enemyScriptableObject.EnemyPointValue);
                //CheckWhetherIncreasePlayerPointScore();
                PlayExplosionSound();
                ExplosionAnimation();
                Destroy(gameObject);
            }
        }
    }

    protected virtual void CheckWhetherIncreasePlayerPointScore()
    {
        if (gameObject.layer == 7)
        {
            playerScoreController.IncreasePlayerScore(enemyScriptableObject.EnemyPointValue);
        }
    }

    public virtual void PlayExplosionSound()
    {
        if (playExplosionSound == true) AudioSource.PlayClipAtPoint(explosionSoundClip, Camera.main.transform.position, explosionSoundVolume);
    }

    public virtual void ExplosionAnimation()
    {
        gameObject.GetComponent<OnDestroyCreateExplosion>().InstantiateExplosion();
    }

    public int ReturnMaxLife()
    {
        return enemyScriptableObject.EnemyHitPoints;
    }

    public int ReturnCurrentLife()
    {
        return lifeNumber;
    }
}
