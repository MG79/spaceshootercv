using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectLifeControllerSpaceStation : ObjectLifeController
{
    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        //je�li collision nie ma komponentu DealDamage (skryptu) to dealDamage b�dzie null-em
        DealDamage dealDamage = collision.GetComponent<DealDamage>();


        if (dealDamage != null)
        {
            lifeNumber = lifeNumber - dealDamage.ReturnDamage();
            if (lifeNumber <= 0)
            {
                if (gameObject.tag == "Enemy") playerScoreController.IncreasePlayerScore(enemyScriptableObject.EnemyPointValue);
                //CheckWhetherIncreasePlayerPointScore();
                PlayExplosionSound();
                ExplosionAnimation();
                Destroy(gameObject.transform.parent.gameObject); //niszczymy parenta
                Destroy(gameObject);
            }
        }
    }

}
