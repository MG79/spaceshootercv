using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//EnemyShoot dziedzieczy z PlayerShoot
public class EnemyShoot : Shoot
{
    [SerializeField] SoEnemyTypeCharacteristic enemyScriptableObject;
    [SerializeField] EnemySoundManager enemySoundManager;

   void Start()
    {
        GetPoolerReferenceInStart();
    }

    private void Update()
    {
        if (isReloading == false)
        {
            ShootMethod();
            isReloading = true;
            StartCoroutine(ReloadTimerCoroutine());
        }
    }

    float RandomizeReloadTime()
    {
        return Random.Range(enemyScriptableObject.WeaponReloadTimeMin, enemyScriptableObject.WeaponReloadTimeMax);
    }

    protected override void GetPoolerReferenceInStart()
    {
        poolerHandle = GameObject.FindGameObjectWithTag("EnemyPooler").GetComponent<Pooler>();
    }

    protected override IEnumerator ReloadTimerCoroutine()
    {
        yield return new WaitForSeconds(RandomizeReloadTime());
        isReloading = false;
    }

    protected override void ShootMethod()
    {
        GameObject projectileFromPooler = poolerHandle.ReturnProjectileFromPooler();
        projectileFromPooler.transform.position = gameObject.transform.position;
        projectileFromPooler.gameObject.SetActive(true);
        enemySoundManager.PlayAudio1();
    }
}
