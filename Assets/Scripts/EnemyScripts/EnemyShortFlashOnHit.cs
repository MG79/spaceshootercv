using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShortFlashOnHit : ObjectShortFlash
{
    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log(collider);
        if(collider.gameObject.tag != "Player")
        {
            FlashOnHit();
        }
    }
}
