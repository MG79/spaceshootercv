using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float playerSpeed = 6f;

    Vector2 playerMovemnetInputVector2;

    //zmienne ograniczające ruch poza ekran
    Vector2 minBounds;
    Vector2 maxBounds;
    [SerializeField] float padding = 0.5f;


    private void Start()
    {
        InitBounds();
    }

    private void Update()
    {
        PlayerMove();
    }

    void InitBounds()
    {
        minBounds = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        maxBounds = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
    }

    //pobranie z UnityEngine.InputSystem na obiekcie gracza wektora inputy OnMove-a
    void OnMove(InputValue value)
    {
        playerMovemnetInputVector2 = value.Get<Vector2>();
    }

    void OnMoveOnTouchScreen(InputValue value)
    {
        playerMovemnetInputVector2 = value.Get<Vector2>() * 0.05f;
    }

    void PlayerMove()
    {
        Vector3 movement = playerMovemnetInputVector2 * playerSpeed * Time.deltaTime;

        Vector2 movementWithBoundaries = new Vector2();

        movementWithBoundaries.x = Mathf.Clamp(transform.position.x + movement.x, minBounds.x + padding, maxBounds.x - padding);
        movementWithBoundaries.y = Mathf.Clamp(transform.position.y + movement.y, minBounds.y + padding, maxBounds.y - padding);

        transform.position = movementWithBoundaries;
    }

}
