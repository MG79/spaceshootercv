using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScoreController : MonoBehaviour
{
    [SerializeField] int playerScorePoints = 0;
    [SerializeField] int pointCapToNextLevel = 50;

    private void Start()
    {
        playerScorePoints = 0;
    }

    public void IncreasePlayerScore(int enemyScoreValue)
    {
        playerScorePoints = playerScorePoints + enemyScoreValue;
    }

    public int ReturnPlayerPointScore()
    {
        return playerScorePoints;
    }
}
