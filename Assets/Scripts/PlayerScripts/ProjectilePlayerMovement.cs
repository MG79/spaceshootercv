using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilePlayerMovement : MonoBehaviour
{
    [SerializeField] float projectileSpeed = 5f;

    private void Update()
    {
        ProjectileMovementMethod();
    }

    void ProjectileMovementMethod()
    {
            transform.position = transform.position + new Vector3(0, 1, 0) * projectileSpeed * Time.deltaTime;
    }
}
