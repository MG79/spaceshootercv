using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI; //biblioteka UI

public class PlayerHelthbarFlashOnHit : ObjectShortFlash
{
    [SerializeField] PlayerLifeController playerLifeController;
    int playerCurrentHealth;

    [SerializeField] Image sliderFill;
    [SerializeField] Image inGameUIPanel;
    [SerializeField] Image topButton;

    [SerializeField] Color flashColorPanel = Color.red;
    [SerializeField] protected Color flashColorTopButton = Color.red;

    Color defaultPanelColor;
    Color defaultTopButton;

    protected override void Start()
    {
        playerCurrentHealth = playerLifeController.ReturnPlayerCurrentLifeCount();
        defaultColor = sliderFill.color;
        defaultPanelColor = inGameUIPanel.color;
        defaultTopButton = topButton.color;
    }

    private void Update()
    {
        if(playerCurrentHealth > playerLifeController.ReturnPlayerCurrentLifeCount())
        {
            playerCurrentHealth = playerLifeController.ReturnPlayerCurrentLifeCount();
            FlashOnHit();
        }
    }

    protected override void OnTriggerEnter2D(Collider2D collider)
    {
    }

    protected override void FlashOnHit()
    {
        StartCoroutine(FlashSliderFill());
        StartCoroutine(FlashInGameMenuPanel());
        StartCoroutine(FlashTopButton());
    }

    IEnumerator FlashSliderFill()
    {
        sliderFill.color = flashColor;
        yield return new WaitForSeconds(flashDuration);
        sliderFill.color = defaultColor;
    }

    IEnumerator FlashInGameMenuPanel()
    {
        
        inGameUIPanel.color = flashColorPanel;
        yield return new WaitForSeconds(flashDuration);
        inGameUIPanel.color = defaultPanelColor;
    }

    IEnumerator FlashTopButton()
    {
        topButton.color = flashColorTopButton;
        yield return new WaitForSeconds(flashDuration);
        topButton.color = defaultTopButton;
    }
}
