using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class PlayerLifeController : MonoBehaviour
{
    [SerializeField] int lifeNumber = 5;
    [SerializeField] int playerMaxLife = 5;
    [SerializeField] GameObject playerVictoryManagerHandle;


    private void Update()
    {
        GameOverDetector();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        DealDamage dealDamage = collision.GetComponent<DealDamage>();

        if (dealDamage != null && playerVictoryManagerHandle.GetComponent<PlayerVictoryController>().PlayerWonBool == false)
        {
            lifeNumber = lifeNumber - dealDamage.ReturnDamage();
            GameOverDetector();
        }
    }

    public int ReturnPlayerCurrentLifeCount()
    {
        return lifeNumber;
    }

    public int ReturnPlayerMaxLifeCount()
    {
        return playerMaxLife;
    }

    private void GameOverDetector()
    {
        if(lifeNumber <= 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}
