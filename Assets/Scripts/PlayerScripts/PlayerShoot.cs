using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : Shoot
{
    [SerializeField] protected float weaponReloadTime = 2f;

    void Start()
    {
        GetPoolerReferenceInStart();
    }

    private void OnFire()
    {
        if(isReloading==false)
        {
            ShootMethod();
            isReloading = true;
            StartCoroutine(ReloadTimerCoroutine());
        }
    }

    protected override void GetPoolerReferenceInStart()
    {
        poolerHandle = gameObject.GetComponent<Pooler>();
    }

    protected override IEnumerator ReloadTimerCoroutine()
    {
        yield return new WaitForSeconds(weaponReloadTime);
        isReloading = false;
    }

    protected override void ShootMethod()
    {
        GameObject projectileFromPooler = poolerHandle.ReturnProjectileFromPooler();
        projectileFromPooler.transform.position = gameObject.transform.position;
        projectileFromPooler.gameObject.SetActive(true);

    }
}
