using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI; //biblioteka UI

public class CutsceneController : MonoBehaviour
{
    [SerializeField] GameObject button1;
    [SerializeField] GameObject button2;

    bool check = true;

    private void Update()
    {
        if(check==true)
        {
            Time.timeScale = 0f;
            check = false;
        }
    }

    public void Dialogue1()
    {
        button1.SetActive(false);
        button2.SetActive(true);
    }

    public void Dialogue2()
    {
        Time.timeScale = 1f;
        button2.SetActive(false);
        Destroy(gameObject);
    }
}
