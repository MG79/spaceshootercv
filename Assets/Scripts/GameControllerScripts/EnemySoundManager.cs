using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySoundManager : SoundManager
{
    private void Start()
    {
        GetReferenceToAudioSourceInStart();
    }

    protected override void GetReferenceToAudioSourceInStart()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    //(odg�os strza�u)
    public override void PlayAudio1()
    {
        audioSource.clip = audioClip1;
        audioSource.volume = volume;
        audioSource.Play();
    }
}
