using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pooler : MonoBehaviour
{
    [SerializeField] GameObject projectilePrefab;

    List<GameObject> poolerList;

    [SerializeField ] int poolerListMaxSize = 10;

    void Start()
    {
        InstantiateInitialPoolerInStart();
    }

    public GameObject ReturnProjectileFromPooler()
    {
        bool inactiveProjectileWasUsed = false;
        int indexOfPollerListToReturn = 0;

        for (int i = 0; i < poolerList.Count && inactiveProjectileWasUsed == false; i++)
        {
            if (poolerList[i].gameObject.activeSelf == false)
            {
                indexOfPollerListToReturn = i;
                inactiveProjectileWasUsed = true;
            }
        }
        if (inactiveProjectileWasUsed == false)
        {
            GameObject newProjectile = Instantiate(projectilePrefab, new Vector3(0, 0, 0), Quaternion.identity);
            newProjectile.SetActive(false);
            poolerList.Add(newProjectile);
            poolerListMaxSize++;
            indexOfPollerListToReturn = poolerList.Count-1;
        }

        return poolerList[indexOfPollerListToReturn];
    }

    protected void InstantiateInitialPoolerInStart()
    {
        poolerList = new List<GameObject>();

        for (int i = 0; i < poolerListMaxSize; i++)
        {
            GameObject newProjectile = Instantiate(projectilePrefab, transform.position + new Vector3(0, 0, 0), Quaternion.identity);
            poolerList.Add(newProjectile);
            poolerList[i].gameObject.SetActive(false);
        }
    }
}
