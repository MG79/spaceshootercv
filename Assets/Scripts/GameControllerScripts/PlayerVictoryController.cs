using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
using TMPro;

public class PlayerVictoryController : MonoBehaviour
{
    [SerializeField] GameObject playerPointsControllerHandle;
    [SerializeField] GameObject vicotoryBanerHandle;
    [SerializeField] int victoryPointCap = 50;

    bool playerWonBool = false;

    public bool PlayerWonBool
    {
        get
        {
            return playerWonBool;
        }
    }

    private void Start()
    {
        Time.timeScale = 1f;
        playerWonBool = false;

        SetLevelNumberInEndLevelScreen();
    }

    private void Update()
    {
        LoadPlayerVicotry();
    }

    private void LoadPlayerVicotry()
    {
        if (playerPointsControllerHandle.GetComponent<PlayerScoreController>().ReturnPlayerPointScore() >= victoryPointCap)
        {
            CheckIfLevelIsUnlocked(); //zapisuje post�p w grze
            playerWonBool = true;
            vicotoryBanerHandle.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    public void victoryButtonMethodEndLevel()
    {
        Time.timeScale = 1f;
        if (SceneManager.GetActiveScene().buildIndex + 1 < SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else
        {
            SceneManager.LoadScene("GameFinished");
        }
    }

    void SetLevelNumberInEndLevelScreen()
    {
        if (SceneManager.GetActiveScene().buildIndex + 1 < SceneManager.sceneCountInBuildSettings)
        {
            vicotoryBanerHandle.GetComponentInChildren<TextMeshProUGUI>().SetText(vicotoryBanerHandle.GetComponentInChildren<TextMeshProUGUI>().text + "\n\nLevel " + (SceneManager.GetActiveScene().buildIndex - 2));
        }        
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // zapis stanu gry (oblokuj kolejny level w menu wyboru poziomu)
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //wywo�ywana w LoadPlayerVicotry()
    void CheckIfLevelIsUnlocked()
    {
        if (SaveController.CheckIfFileExists() == true) //sprawd� czy plik istnieje
        {
            if (SaveController.ReadIntegerFromFile() < (SceneManager.GetActiveScene().buildIndex + 1)) //je�li nast�pny level jest wy�szy ni� najwy�szy odblokowany level zapisany w pliku
            {
                SaveController.WriteIntegerToFile(SceneManager.GetActiveScene().buildIndex + 1); //zapisz, �e odblokowano nast�pny level do pliku
            }
        }
        else //je�eli plik nie istnia�
        {
            SaveController.WriteIntegerToFile(SceneManager.GetActiveScene().buildIndex + 1); //zapisz, �e odblokowano nast�pny level do pliku
        }
    }
}
