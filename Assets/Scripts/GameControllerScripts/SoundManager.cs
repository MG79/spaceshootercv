using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SoundManager : MonoBehaviour
{
    [SerializeField] [Range(0, 1)] protected float volume = 0.2f;
    [SerializeField] protected AudioClip audioClip1;
    protected AudioSource audioSource;


    abstract protected void GetReferenceToAudioSourceInStart();
    abstract public void PlayAudio1();
}
