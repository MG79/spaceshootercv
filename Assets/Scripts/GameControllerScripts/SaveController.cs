using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO; //zawiera biblioteki do operacji na plikach
using System; //zawiera Convert.ToInt32()

public static class SaveController
{
    static string saveDirectoryPath = Application.persistentDataPath + "/SpaceShooterSaveFolder";
    static string saveFilePath = Application.persistentDataPath + "/SpaceShooterSaveFolder/save.txt";

    //sprawdza czy directory do zapisu istnieje, je�li nie to je tworzy
    static void CheckIfDirecotryExistsIfNotCreateOne()
    {
        if(Directory.Exists(saveDirectoryPath)==false)
        {
            Directory.CreateDirectory(saveDirectoryPath);
        }
    }

    //zwraca bool czy plik istnieje
    public static bool CheckIfFileExists()
    {
        bool fileExists=false;
        CheckIfDirecotryExistsIfNotCreateOne();
        if(File.Exists(saveFilePath)==true)
        {
            fileExists = true;
        }

        return fileExists;
    }

    //zwraca najwyzszy odblokowany level z pliku jako int
    //nie sprawdza czy plik istnieje - sprawd� przed wywo�aniem
    public static int ReadIntegerFromFile()
    {
        return Convert.ToInt32(File.ReadAllText(saveFilePath));
    }

    //tworzy lub nadpisuje plik
    //przyjmuje argument int, kt�ry konwertuje na string i zapisuje do pliku
    public static void WriteIntegerToFile(int integer)
    {
        CheckIfDirecotryExistsIfNotCreateOne();

        File.WriteAllText(saveFilePath, integer.ToString());
    }
}
