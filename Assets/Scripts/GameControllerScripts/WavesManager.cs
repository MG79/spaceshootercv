using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WavesManager : MonoBehaviour
{
    [SerializeField] List<SOWaveType0> SOEnemyWaveList;

    //odst�p pomi�dzy falami wrog�w
    //zmienna metody CreateWaves()
    [SerializeField] float timeGapBeetwenWaves = 10f; 

    void Start()
    {
        StartCoroutine(CreateWaves());
    }

    //Tworzy wrog�w w fali
    //Wywo�ywana w  void CreateWaves()
    IEnumerator CreateEnemiesInWaveKorutyna(int waveIndex)
    {
        int enemiesCount;
        enemiesCount = SOEnemyWaveList[waveIndex].EnemyCount;
        Transform enemySpawningPosition = SOEnemyWaveList[waveIndex].ReturnListOfPathPoints()[0].transform;
        //Debug.Log("Enemy spawning point is: ");
        //Debug.Log(enemySpawningPosition);

        for (int i=0;i<enemiesCount;i++)
        {
            Instantiate(SOEnemyWaveList[waveIndex].ReturnRandomizedEnemyPrafab(), new Vector3(enemySpawningPosition.position.x, enemySpawningPosition.position.y, 0), Quaternion.identity); //tworzy wroga
            yield return new WaitForSeconds(SOEnemyWaveList[waveIndex].EnemySpawnGap);
        }
    }

    //Odpala kolejne fale co timeGapBeetwenWaves
    IEnumerator CreateWaves()
    {
        int numberOfWaves = CheckNumberOfWaves();

        for(int i=0; i < numberOfWaves; i++)
        {
            StartCoroutine(CreateEnemiesInWaveKorutyna(i));
            yield return new WaitForSeconds(timeGapBeetwenWaves);
        }
    }


    //Zwraca ilo�� element�w w li�cie SOEnemyWaveList (ilo�c fal do wygenerowania)
    //Wywo�ywane w CreateWaves() 
    int CheckNumberOfWaves()
    {
        int numberOfWaves=0;

        foreach(SOWaveType0 wave in SOEnemyWaveList)
        {
            numberOfWaves++;
        }


        return numberOfWaves;
    }
}
