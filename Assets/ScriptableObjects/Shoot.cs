using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Shoot : MonoBehaviour
{
    [SerializeField] protected float spawnGapOfProjectile = 1;
    protected bool isReloading = false;

    protected Pooler poolerHandle;

    protected abstract void ShootMethod();

    protected abstract IEnumerator ReloadTimerCoroutine();

    protected abstract void GetPoolerReferenceInStart();
}
